### **Description**
android app for counting difference between 2 dates - one is current, one is in the future.  
Output is in various time units.

---
### **Technology**
Android

---
### **Year**
2015

---
### **Screenshot**
![](/README/Screenshot_2018-08-12-20-11-04-093_com.example.iri.as.png)
![](/README/Screenshot_2018-08-12-20-11-11-665_com.example.iri.as.png)
![](/README/Screenshot_2018-08-12-20-11-04-093_com.example.iri.as.png)