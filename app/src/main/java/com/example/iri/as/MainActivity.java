package com.example.iri.as;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    TextView tv_today;
    TextView tv_meeting_date;
    TextView tv_meeting_time;
    Calendar c;
    SimpleDateFormat sdf;

    TextView tv_days;
    TextView tv_hours;
    TextView tv_minutes;
    TextView tv_seconds;

    int year;
    int month;
    int day;
    int today_hour;
    int today_minute;



    int chosen_year;
    int chosen_month;
    int chosen_day;
    int chosen_hour;
    int chosen_minute;

    boolean isSet = false;
    CollapsingToolbarLayout collapsingToolbar;
    AppBarLayout appBarLayout;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.MyToolbar);
        setSupportActionBar(toolbar);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //android.support.v4.widget.NestedScrollView scrollview =  (android.support.v4.widget.NestedScrollView) findViewById(R.id.scrollview);
           // scrollview.setNestedScrollingEnabled(true);
        }

        tv_today = (TextView) findViewById(R.id.today_date);
        tv_meeting_date = (TextView) findViewById(R.id.meeting_date);
        tv_meeting_time = (TextView) findViewById(R.id.meeting_time);

        tv_days = (TextView) findViewById(R.id.days);
        tv_hours = (TextView) findViewById(R.id.hours);
        tv_minutes = (TextView) findViewById(R.id.minutes);
        tv_seconds = (TextView) findViewById(R.id.seconds);
        sdf = new SimpleDateFormat("dd. M. yyyy hh:mm");

        setCurrentDateTime();

        //init values
        chosen_day = day;
        chosen_month = month;
        chosen_year = year;

        chosen_hour = 12;
        chosen_minute = 0;

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        double d_w = size.x;
        double d_h = size.y;

        Drawable d = getResources().getDrawable(R.drawable.y);
        double p_w = d.getIntrinsicWidth();
        double p_h = d.getIntrinsicHeight();

        appBarLayout = (AppBarLayout) findViewById(R.id.MyAppbar);
        double i = ((d_w/p_w) * p_h);
        appBarLayout.setMinimumHeight((int) i);



        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapse_toolbar);
        collapsingToolbar.setTitle("Kolik zbývá času?");
        collapsingToolbar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        collapsingToolbar.setExpandedTitleTextAppearance(R.style.expandedappbar);
        collapsingToolbar.setCollapsedTitleTextAppearance(R.style.collapsedappbar);

        Button date = (Button) findViewById(R.id.btn_date);
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog d = new DatePickerDialog(MainActivity.this, datePickerListener, chosen_year, chosen_month-1, chosen_day);
                d.show();
            }
        });
        tv_meeting_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog d = new DatePickerDialog(MainActivity.this, datePickerListenerAlone, chosen_year, chosen_month-1, chosen_day);
                d.show();
            }
        });

        LinearLayout ll_date = (LinearLayout) findViewById(R.id.ll_date);
        ll_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog d = new DatePickerDialog(MainActivity.this, datePickerListener, chosen_year, chosen_month-1, chosen_day);
                d.show();
            }
        });
        /*
        Button time = (Button) findViewById(R.id.btn_time);
        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialog d = new TimePickerDialog(MainActivity.this, timeSetListener, chosen_hour, chosen_minute, true);
                d.show();
            }
        });
        */
        tv_meeting_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialog d = new TimePickerDialog(MainActivity.this, timeSetListener, chosen_hour, chosen_minute, true);
                d.show();
            }
        });
    }

    private DatePickerDialog.OnDateSetListener datePickerListenerAlone = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            isSet = true;
            chosen_year = selectedYear;
            chosen_month = selectedMonth + 1;
            chosen_day = selectedDay;

            // set selected date into textview
            //collapsingToolbar.setTitle(chosen_day + ". " + chosen_month + ". " + chosen_year + " " + String.format("%02d", chosen_hour) + ":" + String.format("%02d", chosen_minute));
            setCurrentDateTime();
        }
    };

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            isSet = true;
            chosen_year = selectedYear;
            chosen_month = selectedMonth + 1;
            chosen_day = selectedDay;

            // set selected date into textview
            //collapsingToolbar.setTitle(chosen_day + ". " + chosen_month + ". " + chosen_year + " " + String.format("%02d", chosen_hour) + ":" + String.format("%02d", chosen_minute));
            //setCurrentDateTime();
            TimePickerDialog d = new TimePickerDialog(MainActivity.this, timeSetListener, chosen_hour, chosen_minute, true);
            d.show();
        }
    };

    private TimePickerDialog.OnTimeSetListener timeSetListener = new TimePickerDialog.OnTimeSetListener() {

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            isSet = true;
            chosen_hour = hourOfDay;
            chosen_minute = minute;

            setCurrentDateTime();
        }
    };



    private void setCurrentDateTime(){
        c = Calendar.getInstance();

        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH) + 1;
        day = c.get(Calendar.DAY_OF_MONTH);
        today_hour = c.get(Calendar.HOUR_OF_DAY);
        today_minute = c.get(Calendar.MINUTE);

        tv_today.setText(day + ". " + month + ". " + year + " " + String.format("%02d", today_hour) + ":" + String.format("%02d", today_minute));

        long diff_minutes = 0;
        long diff_days = 0;
        long diff_hours = 0;
        long diff_seconds = 0;
        if(isSet) {
            tv_meeting_date.setText(chosen_day + ". " + chosen_month + ". " + chosen_year);
            tv_meeting_time.setText(String.format("%02d", chosen_hour) + ":" + String.format("%02d", chosen_minute));
            Date d1 = new Date(year, month, day);
            Date d2 = new Date(chosen_year, chosen_month, chosen_day);
            try {
                d1 = sdf.parse(tv_today.getText().toString());
                d2 = sdf.parse(tv_meeting_date.getText().toString() + " " + tv_meeting_time.getText().toString());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            diff_minutes = count(d1, d2, TimeUnit.MINUTES);
            diff_days = count(d1, d2, TimeUnit.DAYS);
            diff_hours = count(d1, d2,  TimeUnit.HOURS);
            diff_seconds = count(d1, d2, TimeUnit.SECONDS);

            //collapsingToolbar.setTitle("Zbývá " + diff_minutes + " minut");
            tv_days.setText(String.valueOf(diff_days));
            tv_hours.setText(String.valueOf(diff_hours));
            tv_minutes.setText(String.valueOf(diff_minutes));
            tv_seconds.setText(String.valueOf(diff_seconds));

            Map<TimeUnit,Long> date_map = computeDiff(d1, d2);

            collapsingToolbar.setTitle("Zbývá: " + diff_minutes + " (" + date_map.get(TimeUnit.DAYS) + "d, " +
                    date_map.get(TimeUnit.HOURS) + "h, " +
                    date_map.get(TimeUnit.MINUTES) + "m, " +
                    date_map.get(TimeUnit.SECONDS) + "s)");

            Snackbar.make(this.findViewById(android.R.id.content),
                    "Zbývá " + date_map.get(TimeUnit.DAYS) + " dnů, " +
                            date_map.get(TimeUnit.HOURS) + " hodin, " +
                            date_map.get(TimeUnit.MINUTES) + " minut, " +
                            date_map.get(TimeUnit.SECONDS) + " vteřin.", Snackbar.LENGTH_LONG)
            .show();
        }
    }

    public static long count(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }

    public static Map<TimeUnit,Long> computeDiff(Date date1, Date date2) {
        long diffInMillies = date2.getTime() - date1.getTime();
        List<TimeUnit> units = new ArrayList<TimeUnit>(EnumSet.allOf(TimeUnit.class));
        Collections.reverse(units);
        Map<TimeUnit,Long> result = new LinkedHashMap<TimeUnit,Long>();
        long milliesRest = diffInMillies;
        for ( TimeUnit unit : units ) {
            long diff = unit.convert(milliesRest,TimeUnit.MILLISECONDS);
            long diffInMilliesForUnit = unit.toMillis(diff);
            milliesRest = milliesRest - diffInMilliesForUnit;
            result.put(unit,diff);
        }
        return result;
    }

    @Override
    public void onPause(){
        super.onPause();
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean("isSet", isSet);
        if(isSet) {
            editor.putInt("chosen_year", chosen_year);
            editor.putInt("chosen_month", chosen_month);
            editor.putInt("chosen_day", chosen_day);
            editor.putInt("chosen_hour", chosen_hour);
            editor.putInt("chosen_minute", chosen_minute);
        }
        editor.commit();

    }

    @Override
    public void onResume(){
        super.onResume();
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        isSet = sharedPref.getBoolean("isSet", isSet);
        if(isSet) {
            chosen_year = sharedPref.getInt("chosen_year", chosen_year);
            chosen_month = sharedPref.getInt("chosen_month", chosen_month);
            chosen_day = sharedPref.getInt("chosen_day", chosen_day);
            chosen_hour = sharedPref.getInt("chosen_hour", chosen_hour);
            chosen_minute = sharedPref.getInt("chosen_minute", chosen_minute);
        }
        setCurrentDateTime();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
